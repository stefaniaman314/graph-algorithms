import java.util.Iterator;
import java.util.LinkedList;
import java.util.Stack; 

public class Graph {
	
	private int V; 
    private LinkedList<Integer> adjListArray[]; 
      
    /**
     * Constructor 
     * sets number of vertices
     * defines the size of the array as number of vertices
     * creates a new list for each vertex such that adjacent nodes can be stored 
     * @param V
     */
    Graph(int V) 
    { 
        this.V = V; 
        
        adjListArray = new LinkedList[V]; 
          
        for(int i = 0; i < V ; i++){ 
            adjListArray[i] = new LinkedList<>(); 
        } 
    } 
    
    /**
     * Method
     * adds an edge to the graph
     * @param src
     * @param dest
     * @return void
     */
    void addEdge(int src, int dest) 
    {  
        this.adjListArray[src].add(dest);
        
        this.adjListArray[dest].add(src);
    }
    
    /**
     * Method
     * prints the adjacency list representation of the graph 
     * @return void
     */
    void printGraph() 
    {        
        for(int v = 0; v < this.V; v++) 
        { 
            System.out.println("Adjacency list of vertex "+ v); 
            System.out.print("head");
            
            for(Integer pCrawl: this.adjListArray[v])
            { 
                System.out.print(" -> "+pCrawl); 
            } 
            
            System.out.println("\n"); 
        } 
    }
    
    /**
     * Method
     * prints connected components in the graph
     * @return void
     */
    void connectedComponents()
    {
    	boolean visited[] = new boolean[V];
    	
    	//Mark all vertices as not visited
    	for(int v = 0; v < V; v++)
    		visited[v] = false;
    	
    	for(int v = 0; v < V; v++)
    	{
    		if(visited[v] == false)
    		{
    			//Print all reachable vertices from node v
    			DFSUtil(v, visited);
    			
    			System.out.println("");
    		}
    	}
    }
       
    /**
     * Method
     * prints strongly connected components in the graph
     * @return void
     */
    void stronglyCnnectedComponents() 
    { 
        Stack<Integer> stack = new Stack<Integer>(); 
  
        //Mark all the vertices as not visited (for first DFS) 
        boolean visited[] = new boolean[V]; 
        for(int i = 0; i < V; i++) 
            visited[i] = false; 
  
        //Fill vertices in stack according to their finishing times 
        for (int i = 0; i < V; i++) 
            if (visited[i] == false) 
                topologicalSortUtil(i, visited, stack); 
  
        //Create a reversed graph 
        Graph gr = getTranspose(); 
  
        //Mark all the vertices as not visited (for second DFS) 
        for (int i = 0; i < V; i++) 
            visited[i] = false; 
  
        //Now process all vertices in order defined by Stack 
        while (stack.empty() == false) 
        { 
            //Pop a vertex from stack 
            int v = (int)stack.pop(); 
  
            //Print strongly connected component of the popped vertex 
            if (visited[v] == false) 
            { 
                gr.DFSUtil(v, visited); 
                System.out.println(); 
            } 
        } 
    } 
    
    /**
     * Method
     * recursively prints DFS starting from node v
     * @param v
     * @param visited
     */
    void DFSUtil(int v, boolean visited[])
    {
    	//Mark the current node as visited and print it
    	visited[v] = true;
    	System.out.print(v + " ");
    	
    	int n; 
    	  
        //Recur for all the vertices adjacent to this vertex 
        Iterator<Integer> i =adjListArray[v].iterator(); 
        while (i.hasNext()) 
        { 
            n = i.next(); 
            
            if (!visited[n]) 
                DFSUtil(n,visited); 
        } 
    }
    
    /**
     * Method
     * returns reverse of the graph
     * @return reversed graph
     */
    Graph getTranspose() 
    { 
        Graph g = new Graph(V); 
        
        for (int v = 0; v < V; v++) 
        { 
            //Recur for all the vertices adjacent to this vertex 
            Iterator<Integer> i =adjListArray[v].listIterator(); 
           
            while(i.hasNext()) 
                g.adjListArray[i.next()].add(v); 
        } 
        
        return g; 
    } 
    
    /**
     * Method
     * does the topological sort 
     * using topologicalSortUtil helper function
     * @return void
     */
    void topologicalSort() 
    { 
        Stack<Integer> stack = new Stack<Integer>(); 
  
        // Mark all the vertices as not visited 
        boolean visited[] = new boolean[V]; 
        for (int i = 0; i < V; i++) 
            visited[i] = false; 
  
        // Call the recursive helper function 
        // to store all adjacent nodes    
        for (int i = 0; i < V; i++) 
            if (visited[i] == false) 
                topologicalSortUtil(i, visited, stack); 
  
        // Print contents of stack 
        while (stack.empty()==false) 
            System.out.print(stack.pop() + " "); 
    } 
    
    /**
     * Method (recursive)
     * visits all adjacent nodes starting from node v
     * and pushes them to the stack
     * according to their finishing time
     * @param v
     * @param visited
     * @param stack
     * @return void
     */
    void topologicalSortUtil(int v, boolean visited[], Stack<Integer> stack) 
    { 
        //Mark the current node as visited. 
        visited[v] = true; 
        Integer i; 
  
        //Recur for all the vertices adjacent to this vertex 
        Iterator<Integer> it = adjListArray[v].iterator(); 
        while (it.hasNext()) 
        { 
            i = it.next(); 
            if (!visited[i]) 
                topologicalSortUtil(i, visited, stack); 
        } 
  
        // Push current vertex to stack which stores result 
        stack.push(new Integer(v)); 
    } 
}
