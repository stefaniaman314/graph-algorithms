package main;

import java.util.Arrays;

public class Graph {

	//A nested class used to represent a graph edge
	class Edge implements Comparable<Edge>
	{
		public int src;
		public int dest;
		public int weight;
		
		@Override
		public int compareTo(Edge compareEdge) 
		{
			return this.weight - compareEdge.weight;
		}
	}
	
	//A nested class used to represent a subset for union-find
	class Subset
	{
		public int parent;
		public int rank;
	}
	
	public int V; 			//number of vertices
	public int E;			//number of edges
	public Edge edges[]; 	//collection of all edges
	
	public Graph(int v, int e)
	{
		this.V = v;
		this.E = e;
		edges = new Edge[E];
		
		for(int index = 0; index < e; ++index)
			edges[index] = new Edge();
	}
	
	//A function that finds a set of an element index
	//(uses path compression technique)
	public int find(Subset subsets[], int index)
	{
		//find root and make it parent of element index
		if(subsets[index].parent != index)
			subsets[index].parent = find(subsets, subsets[index].parent);
		
		return subsets[index].parent;
	}
	
	//A function that does union of two sets x and y
	//(uses union-by-rank)
	public void union(Subset subsets[], int x, int y)
	{
		int xRoot = find(subsets, x);
		int yRoot = find(subsets, y);
		
		if(xRoot == yRoot)
			return;
		
		//Attach smaller rank tree under root of higher rank tree
		else if(subsets[xRoot].rank < subsets[yRoot].rank)
			subsets[xRoot].parent = yRoot;
		
		else if(subsets[xRoot].rank > subsets[yRoot].rank)
			subsets[yRoot].parent = xRoot;
		
		//If ranks are equal, make one as root and increment its rank
		else
		{
			subsets[yRoot].parent = xRoot;
			++subsets[xRoot].rank;
		}
	}
	
	public void KruskalMST()
	{	
		//This will store the result MST
		Edge result[] = new Edge[V];
		for(int index = 0; index < V; ++index)
			result[index] = new Edge();
		
		//Sort all edges after their weight
		Arrays.sort(edges);
		
		//Create V subsets with single elements
		Subset subsets[] = new Subset[V];
		for(int index = 0; index < V; ++index)
			subsets[index] = new Subset();
		
		for(int v = 0; v < V; ++v)
		{
			subsets[v].parent = v;
			subsets[v].rank = 0;
		}
		
		int edge = 0;  //keeps track of edges in result
		int index = 0; //index used to pick next edge
		
		while(edge < V - 1)
		{
			//Pick the smallest edge and increment the index
			Edge nextEdge = new Edge();
			nextEdge = edges[index++];
			
			int x = find(subsets, nextEdge.src);
			int y = find(subsets, nextEdge.dest);
			
			//If adding this edge doesn't form a cycle
			if(x != y)
			{
				//include it in result and increment the edge index
				result[edge++] = nextEdge;
				union(subsets, x, y);
			}
			//else discard it
		}
		
		// Print the contents of result to display MSP
        System.out.println("Following are the edges in MST: ");
        
        for (index = 0; index < edge; ++index) 
        {
        	System.out.println( result[index].src+" -- " +  
            					result[index].dest+" == " + 
            					result[index].weight); 
        }
	}
	
	
}
