package main;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import java.io.File;
import java.util.Scanner;

public class Graf {
	private static void initUI(int[][] matrix) {
		JFrame f = new JFrame("Algoritmica Grafurilor");
		// sa se inchida aplicatia atunci cand inchid fereastra
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// imi creez ob MyPanel
		f.add(new MyPanel(matrix));
		// f.add(new MyPanel());
		// setez dimensiunea ferestrei
		f.setSize(500, 500);
		// fac fereastra vizibila
		f.setVisible(true);
	}

	public static void main(String[] args) throws Exception {
		
		int[][] matrix;
		int size;
		
		File myFile = new File("matrix.txt");
		Scanner scanFile = new Scanner(myFile);
		size = scanFile.nextInt();
		matrix = new int[size][size];
		
		while (scanFile.hasNextLine()) {
			for (int i = 0; i < size; ++i)
				for (int j = 0; j < size; ++j)
					matrix[i][j] = scanFile.nextInt();	
		}

		SwingUtilities.invokeLater(new Runnable() // new Thread()
		{
			public void run() {
				initUI(matrix);
			}
		});
	}
}
