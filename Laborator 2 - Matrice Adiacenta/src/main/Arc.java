package main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.geom.Line2D;

public class Arc {
	private Point start;
	private Point end;

	public Arc(Point start, Point end) {
		this.start = start;
		this.end = end;
	}

	public void drawArc(Graphics g) {
		if (start != null) {
			g.setColor(Color.BLUE);
			Graphics2D g2 = (Graphics2D) g;
			g2.draw(new Line2D.Double(start.x, start.y, end.x, end.y));
		}
	}
}
