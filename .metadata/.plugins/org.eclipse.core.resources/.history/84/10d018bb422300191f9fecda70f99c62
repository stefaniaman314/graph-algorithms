package main;

import java.util.LinkedList; 

public class MaxFlow {

	int V;

	public MaxFlow(int V) {
		this.V = V;
	}
	
	/**
	 * @param rGraph residual graph
	 * @param s source vertex
	 * @param t sink vertex
	 * @param parent used to store the path
	 * @return true if there is a path from s to t in residual graph
	 */
	boolean BFS(int rGraph[][], int s, int t, int parent[]) 
	{
		boolean visited[] = new boolean[V];
		for(int i = 0; i < V; ++i)
			visited[i] = false;
		
		LinkedList<Integer> queue = new LinkedList<Integer>();
		queue.add(s);
		visited[s] = true;
		parent[s] = -1;
		
		while(queue.size() != 0)
		{
			int u = queue.poll();
			
			for(int v = 0; v < V; v++)
			{
				if(visited[v] == false && rGraph[u][v] > 0)
				{
					queue.add(V);
					parent[v] = u;
					visited[v] = true;
				}
			}
		}
		
		return (visited[t] == true);
	}
	
	/**
	 * @param graph
	 * @param s source vertex
	 * @param t sink vertex
	 * @return maximum flow from s to t in given graph
	 */
	int fordFulkerson(int graph[][], int s, int t)
	{
		int u, v;
		
		//Create a residual graph
		int rGraph[][] = new int[V][V];
		
		//Initialize residual capacities in residual graph
		//with given capacities in the original graph
		for(int i = 0; i < V; ++i)
			for(int j = 0; j < V; ++j)
				rGraph[i][j] = graph[i][j];
		
		//Array to store path, filled by BFS.
		int parent[] = new int[V];
		
		//Initialize the flow
		int max_flow = 0;
		
		//Increase the flow while there is path from s to t
		while(BFS(rGraph, s, t, parent))
		{
			//Find the minimum residual capacity along the path
			int path_flow = Integer.MAX_VALUE;
			for(v = t; v != s; v = parent[v])
			{
				u = parent[v];
				path_flow = Math.min(path_flow, rGraph[u][v]);
			}
			
			//Update residual capacities 
			//and reverse edges along the path
			for(v = t; v != s; v = parent[v])
			{
				u = parent[v];
				rGraph[u][v] -= path_flow;
				rGraph[v][u] += path_flow;
			}
			
			//Add path flow to overall flow
			max_flow += path_flow;
		}
		
		return max_flow;
	}
}
