package main;

import java.util.List;

public class DepthFirst {
	
    /**
     * @param maze
     * @param x (start position coordX)
     * @param y (start position coordY)
     * @param path (a list which will be filled  with nodes in reverse order)
     * @return true (if path was found)
     */
    public static boolean searchPath(int[][] maze, int x, int y, List<Integer> path) {
        
    	//if the exit is found
        if (maze[x][y] == 3) { 
            path.add(x);
            path.add(y);
            return true;
        }
        
        if (isSafe(maze, x, y) == true && maze[x][y] == 0) {
            
        	maze[x][y] = 4; //mark as visited
            
            //visit all neighbors recursively
            if (searchPath(maze, x - 1, y, path)) {
                path.add(x);
                path.add(y);
                return true;
            }

            if (searchPath(maze, x + 1, y, path)) {
                path.add(x);
                path.add(y);
                return true;
            }
    
            if (searchPath(maze, x, y - 1, path)) {
                path.add(x);
                path.add(y);
                return true;
            }

            if (searchPath(maze, x, y + 1, path)) {
                path.add(x);
                path.add(y);
                return true;
            }
        }
        return false;
    }
    
     //A utility function to check if x,y is valid index for maze 
	static boolean isSafe(int maze[][], int x, int y) 
	{ 
	    // if x,y are outside maze return false 
	    return (x >= 0 && x < maze.length && y >= 0 && 
	            y < maze.length && maze[x][y] == 1); 
	} 
  
}